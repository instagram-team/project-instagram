package com.introlab.instagram.controller;

import com.introlab.instagram.domain.*;
import com.introlab.instagram.service.*;
import lombok.Synchronized;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.ServletContextAware;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.WebContext;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

/**
 * @author vitalii.
 */
@Controller
@RequestMapping("instagram")
public class InstagramController {

    private final InstagramUserService userService;

    private final SearchQueryService queryService;

    private final ExportService exportService;

    private final BaseUserService baseUserService;

    private final WorkerService workerService;

    private final DropletService dropletService;

    @Autowired
    public InstagramController(SearchQueryService queryService, InstagramUserService userService, ExportService exportService, BaseUserService baseUserService, WorkerService workerService, DropletService dropletService) {
        this.queryService = queryService;
        this.userService = userService;
        this.exportService = exportService;
        this.baseUserService = baseUserService;
        this.workerService = workerService;
        this.dropletService = dropletService;
    }

    @RequestMapping("status")
    @ResponseBody
    public List<SearchQuery> status() {
        return queryService.findAll();
    }

    @RequestMapping("tokens")
    @ResponseBody
    public List<Token> tokens() {
        return workerService.getAllTokens();
    }

    @RequestMapping("droplets")
    @ResponseBody
    public List<Droplet> droplets() {
        return dropletService.listDropletsDataBase();
    }

    @RequestMapping("status/users")
    @ResponseBody
    public Map<String, String> statusUsers() {
        HashMap<String, String> map = new HashMap<>();
        for (SearchQuery searchQuery : queryService.findAll()) {
            map.put(searchQuery.getTags(), String.valueOf(userService.countByQueryId(searchQuery) + " of " + searchQuery.getUserCount()));
        }
        return map;
    }

    @RequestMapping("status/chartOne")
    @ResponseBody
    public Map<String, Long> chartOne() {
        HashMap<String, Long> map = new HashMap<>();
        for (SearchQuery searchQuery : queryService.findAll()) {
            map.put(searchQuery.getTags(), userService.countByQueryId(searchQuery));
        }
        return map;
    }

    @RequestMapping("statistic")
    @ResponseBody
    public Statistic statistic() {
        Long count = userService.count();
        Long emailExists = userService.countDistinctByEmailExists();
        Statistic statistic = new Statistic();
        statistic.setAllUsersCount(count);
        statistic.setAllWithEmails(emailExists);
        statistic.setSubStatistics(new ArrayList<>());

        for (SearchQuery searchQuery : queryService.findAll()) {
            Long countByQueryId = userService.countByQueryId(searchQuery);
            Long byEmailExistsAndQueryId = userService.countDistinctByEmailExistsAndQueryId(searchQuery);
            SubStatistic subStatistic = new SubStatistic();
            subStatistic.setAllUsersCount(countByQueryId);
            subStatistic.setTag(searchQuery.getTags());
            subStatistic.setWithEmails(byEmailExistsAndQueryId);
            statistic.getSubStatistics().add(subStatistic);
        }
        return statistic;
    }

    @RequestMapping("emailstatistic")
    @ResponseBody
    public List<SubStatistic> emailStatistic() {
        List<SubStatistic> subStatistics = new ArrayList<>();
        for (SearchQuery searchQuery : queryService.findAll()) {
            Long countByQueryId = userService.countByQueryId(searchQuery);
            Long byEmailExistsAndQueryId = userService.countDistinctByEmailExistsAndQueryId(searchQuery);
            SubStatistic subStatistic = new SubStatistic();
            subStatistic.setAllUsersCount(countByQueryId);
            subStatistic.setTag(searchQuery.getTags());
            subStatistic.setWithEmails(byEmailExistsAndQueryId);
            subStatistics.add(subStatistic);
        }
        return subStatistics;
    }

    @RequestMapping("remove/{queryId}")
    public String remove(@PathVariable String queryId) {
        baseUserService.deleteByQueryId(queryId);
        userService.deleteByQueryId(queryId);
        queryService.delete(queryId);
        return "redirect:/exports";
    }

    @RequestMapping("export/{queryId}")
    @ResponseBody
    public String export(@PathVariable String queryId) {

        SearchQuery searchQuery = queryService.findById(queryId);
        new Thread(() -> {
            List<InstagramUser> instagramUsers = userService.findByQuery(searchQuery);
            exportService.exportUsers(instagramUsers, searchQuery.getTags());
        }).start();
        return "Export started for tag: " + searchQuery.getTags();
    }

    @RequestMapping("exportemailsonly/{queryId}")
    @ResponseBody
    public String exportWithEmailsOnly(@PathVariable String queryId) {

        SearchQuery searchQuery = queryService.findById(queryId);
        new Thread(() -> {
            List<InstagramUser> instagramUsers = userService.findByQueryWithEmails(searchQuery);
            exportService.exportUsers(instagramUsers, searchQuery.getTags());
        }).start();
        return "Export started for tag: " + searchQuery.getTags();
    }

    @RequestMapping("droplet/remove/{dropletId}")
    public String dropDroplet(@PathVariable Long dropletId) throws IOException {

        dropletService.removeDroplet(dropletId);
        workerService.releaseTokens(dropletId);
        return "redirect:/managedroplets";
    }

    @RequestMapping("droplet/remove/all")
    public String dropDroplets() throws IOException {

        for (Droplet droplet : dropletService.listDropletsDataBase()) {
            dropletService.removeDroplet(droplet.getId());
            dropletService.removeFromDB(droplet);
            workerService.releaseTokens(droplet.getId());
        }
        workerService.setRunned(false);
        return "redirect:/managedroplets";
    }

    @RequestMapping("token/release/all")
    public String releaseTokens() throws IOException {
        workerService.releaseAllTokens();
        return "redirect:/managetokens";
    }

    @RequestMapping("scrape/{tag}")
    @ResponseBody
    public String addTag(@PathVariable String tag) {
        SearchQuery searchQueryyId = new SearchQuery();
        searchQueryyId.setTags(tag);
        searchQueryyId.setId("tag_id_" + Math.abs(tag.hashCode()));
        queryService.save(searchQueryyId);
        workerService.startWorkers();
        return "Tag added for scraping!";
    }

    @RequestMapping(value = "scrape", method = RequestMethod.POST)
    public String addTagPost(@RequestParam String tag, RedirectAttributes attributes) {

        String generatedId = "tag_id_" + Math.abs(tag.hashCode());
        if (queryService.findById(generatedId) != null) {
            attributes.addFlashAttribute("isexist", true);
            return "redirect:/home";
        }
        SearchQuery searchQueryyId = new SearchQuery();
        searchQueryyId.setTags(tag);
        searchQueryyId.setId(generatedId);
        queryService.save(searchQueryyId);
        new Thread(workerService::startWorkers).start();
        return "redirect:/home";
    }

    @RequestMapping(value = "tokens/add", method = RequestMethod.POST)
    public String addToken(@RequestParam String token) {
        Token accessToken = new Token();
        accessToken.setToken(token);
        workerService.saveToken(accessToken);
        return "redirect:/managetokens";
    }

    @RequestMapping("pause/{queryId}")
    @ResponseBody
    public String pause(@PathVariable String queryId) {
        SearchQuery searchQueryId = queryService.findById(queryId);
        searchQueryId.setPause(true);
        queryService.save(searchQueryId);
        return "Scraping paused for tag " + searchQueryId.getTags();
    }

    @RequestMapping("resume/{queryId}")
    @ResponseBody
    public String resume(@PathVariable String queryId) {
        SearchQuery searchQueryId = queryService.findById(queryId);
        searchQueryId.setPause(false);
        queryService.save(searchQueryId);
        return "Scraping resumed for tag " + searchQueryId.getTags();
    }

    @RequestMapping(value = "/baseusers", method = RequestMethod.GET)
    @ResponseBody
    @Synchronized
    public List<BaseUser> getBaseUsersPack() {
        List<BaseUser> baseUsers = baseUserService.getPackForScraping();
        if (baseUsers != null) {
            return baseUsers;
        } else {
            return Collections.emptyList();
        }
    }

    @RequestMapping(value = "/updatenames", method = RequestMethod.GET)
    public String updateNames() {
        new Thread(userService::updateNames).start();
        return "redirect:/home";
    }


    @RequestMapping(value = "/chart", method = RequestMethod.GET)
    @ResponseBody
    public String chart() {
        return "[\n" +
                "  {\n" +
                "    \"timestamp\": \"Monday\",\n" +
                "    \"original_tweet\": \"756\",\n" +
                "    \"retweets\": \"345\",\n" +
                "    \"shared\": \"34\",\n" +
                "    \"quoted\": \"14\"\n" +
                "  },\n" +
                "  {\n" +
                "    \"timestamp\": \"Tuesday\",\n" +
                "    \"original_tweet\": \"756\",\n" +
                "    \"retweets\": \"345\",\n" +
                "    \"shared\": \"34\",\n" +
                "    \"quoted\": \"14\"\n" +
                "  },\n" +
                "  {\n" +
                "    \"timestamp\": \"Wednesday\",\n" +
                "    \"original_tweet\": \"756\",\n" +
                "    \"retweets\": \"345\",\n" +
                "    \"shared\": \"34\",\n" +
                "    \"quoted\": \"14\"\n" +
                "  }\n" +
                "]";
    }
}
