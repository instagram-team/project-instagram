package com.introlab.instagram.rpository;

import com.introlab.instagram.domain.InstagramUser;
import com.introlab.instagram.domain.SearchQuery;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * @author vitalii.
 */
public interface InstagramUserRepository extends MongoRepository<InstagramUser, Long> {

    @Query(value = "{tag._id : ?0}", count = true)
    Long countByTag(String query);

    @Query("{tag._id : ?0}")
    List<InstagramUser> findByTag(String taId);

    @Query(value = "{tag._id : ?0}", fields="{ 'medias': 0}")
    List<InstagramUser> findByTagForExport(String taId);

    @Query(value = "{$and: [{ 'email' : {$ne: \"\"}}, {'tag._id' : ?0}]}",count = true)
    Long countDistinctByEmailContainsAndTag(String query);

    @Query(value = "{$and: [{ 'email' : {$ne: \"\"}}, {'tag._id' : ?0}]}",fields = "{ 'medias': 0}")
    List<InstagramUser> dstinctByEmailContainsAndTag(String query);
    @Query(value = "{ 'email' : {$ne: \"\"}}",count = true)
    Long countDistinctByEmailContains();
    Long countAllById();

    @Query(value = "{tag._id : ?0}", delete = true)
    void deleteByQueryId(String query);


}
