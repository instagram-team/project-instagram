package com.introlab.instagram.rpository;

import com.introlab.instagram.domain.SearchQuery;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

/**
 * @author vitalii.
 */
public interface SearchQueryRepository extends MongoRepository<SearchQuery, String> {

    SearchQuery findFirstByStarted(boolean started);
    List<SearchQuery> findByFinished(boolean finished);
}
