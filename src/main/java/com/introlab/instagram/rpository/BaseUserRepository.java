package com.introlab.instagram.rpository;

import com.introlab.instagram.domain.BaseUser;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;

/**
 * @author vitalii.
 */
public interface BaseUserRepository extends MongoRepository<BaseUser, Long> {

    @Query(value = "{tag._id : ?0}", count = true)
    Long countByTag(String query);

    @Query(value = "{tag._id : ?0}", delete = true)
    void deleteByQueryId(String query);

    List<BaseUser> findFirst100ByScraped(boolean scraped);
    List<BaseUser> findFirst10ByScraped(boolean scraped);

    List<BaseUser> findAllByScraped(boolean scraped);

}
