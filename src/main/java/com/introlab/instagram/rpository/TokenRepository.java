package com.introlab.instagram.rpository;

import com.introlab.instagram.domain.Token;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

/**
 * @author vitalii.
 */
public interface TokenRepository extends MongoRepository<Token, String> {
    Token findFirstByInUse(boolean inUse);
    List<Token> findFirst10ByInUse(boolean inUse);
    List<Token> findByDropletId(Long dropletId);
}
