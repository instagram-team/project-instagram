package com.introlab.instagram.rpository;

import com.introlab.instagram.domain.Droplet;
import com.introlab.instagram.domain.Token;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * @author vitalii.
 */
public interface DropletRepository extends MongoRepository<Droplet, Long> {

    Droplet findById(String ip);
}
