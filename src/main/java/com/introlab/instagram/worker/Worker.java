package com.introlab.instagram.worker;

import com.introlab.instagram.domain.Droplet;
import com.introlab.instagram.domain.SearchQuery;
import com.introlab.instagram.domain.Token;
import com.introlab.instagram.observers.Observable;
import com.introlab.instagram.observers.Observer;
import com.introlab.instagram.service.SearchQueryService;
import com.introlab.instagram.service.WorkerService;
import com.introlab.instagram.utils.InstagramUtils;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @author vitalii.
 */
@Component
@Getter
@Setter
public class Worker implements Observable {

    private List<Observer> observers = new ArrayList<>();

    private int maxTokensCount;
    private int maxTagsCount;
    private String workerIp;
    private Droplet droplet;
    private List<SearchQuery>searchQueries;
    private List<Token> tokens;

    @Autowired
    private WorkerService workerService;

    @Autowired
    private SearchQueryService searchQueryService;

//    public void init(){
//        String myIpV4 = InstagramUtils.getMyIpV4();
//        droplet=workerService.getDropletByIp(myIpV4);
//
//        searchQueries = new ArrayList<>();
//        for (int i = 0; i < maxTagsCount; i++) {
//            SearchQuery searchQuery = searchQueryService.getAvailableSearchQuery(droplet.getId());
//            if (searchQuery!=null){
//                searchQueries.add(searchQuery);
//                notifyObservers(searchQuery);
//            }
//        }
//
//        tokens = new ArrayList<>();
//        for (int i = 0; i < maxTokensCount; i++) {
//            Token token = workerService.getAvailableToken(droplet.getId());
//            if (token!=null){
//                tokens.add(token);
//            }
//        }
//        //ToDO: run profile scraper
//
//
//    }

    @Override
    public void registerObserver(Observer o) {
        observers.add(o);
    }

    @Override
    public void removeObserver(Observer o) {
        observers.remove(o);
    }

    @Override
    public void notifyObservers(SearchQuery searchQuery) {
        for (Observer observer : observers)
            observer.update(searchQuery);
    }
}
