package com.introlab.instagram.service;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.introlab.instagram.domain.Droplet;
import com.introlab.instagram.rpository.DropletRepository;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.jsoup.Connection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Collections;
import java.util.List;

import static java.lang.Thread.sleep;

/**
 * @author vitalii.
 */
@Service
public class DropletService {

    private final DropletRepository dropletRepository;

    @Autowired
    public DropletService(DropletRepository dropletRepository) {
        this.dropletRepository = dropletRepository;
    }

    public void removeDroplet(Long dropletId) throws IOException {
        URL url = new URL("https://api.digitalocean.com/v2/droplets/" + dropletId);
        HttpURLConnection httpCon = (HttpURLConnection) url.openConnection();
        httpCon.setDoOutput(true);
        httpCon.setRequestProperty("Content-Type", "application/json");
        httpCon.setRequestProperty("Authorization", "Bearer ed3b42be2ee505a14ab8eb489c2ae36cfcffd6b1700ebb5d257ad79c1a3d901a");
        httpCon.setRequestMethod("DELETE");
        httpCon.connect();
        System.out.println("Response code: " + httpCon.getResponseCode());
    }

    public void removeFromDB(Droplet droplet) throws IOException {
        dropletRepository.delete(droplet);
    }


    public Droplet createDroplet(String name) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.add("Authorization", "Bearer ed3b42be2ee505a14ab8eb489c2ae36cfcffd6b1700ebb5d257ad79c1a3d901a");

        String body = "{\"image\":26793727,\"ssh_keys\":[\"9c:37:a6:ab:11:e5:c8:eb:42:b8:aa:ee:41:d8:7c:e0\"],\"size\":\"2gb\",\"name\":\"" + name + "\",\"region\":\"fra1\"}";
        String url = "https://api.digitalocean.com/v2/droplets";
        HttpEntity<String> request = new HttpEntity<>(body, headers);
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.POST, request, String.class);
        JsonElement jsonElement = new JsonParser().parse(responseEntity.getBody());
        JsonObject dropletObject = jsonElement.getAsJsonObject().getAsJsonObject("droplet");
        long id = dropletObject.getAsJsonPrimitive("id").getAsLong();
        return getDroplet(id);
    }

    public List<Droplet> listDropletsApi() throws IOException {
        HttpClient client = HttpClientBuilder.create().build();
        HttpGet request = new HttpGet("https://api.digitalocean.com/v2/droplets?page=1");
        request.setHeader(HttpHeaders.CONTENT_TYPE, "application/json");
        request.setHeader("Authorization", "Bearer ed3b42be2ee505a14ab8eb489c2ae36cfcffd6b1700ebb5d257ad79c1a3d901a");
        HttpResponse response = client.execute(request);
        String json = EntityUtils.toString(response.getEntity());
        System.out.println(json);
        return null;
    }

    public List<Droplet> listDropletsDataBase() {
        List<Droplet> droplets = dropletRepository.findAll();
        return droplets != null && !droplets.isEmpty() ? droplets : Collections.emptyList();
    }

    private Droplet parseDroplet(String json) {
        JsonElement jsonElement = new JsonParser().parse(json);
        JsonObject dropletObject = jsonElement.getAsJsonObject().getAsJsonObject("droplet");
        long id = dropletObject.getAsJsonPrimitive("id").getAsLong();
        String name = dropletObject.getAsJsonPrimitive("name").getAsString();


        JsonObject networks = dropletObject.getAsJsonObject("networks");
        JsonArray asJsonArray = networks.getAsJsonArray("v4");
        if (asJsonArray.size() == 0) {
            return null;
        }
        String ip = asJsonArray.get(0).getAsJsonObject().getAsJsonPrimitive("ip_address").getAsString();
        Droplet droplet = new Droplet();
        droplet.setId(id);
        droplet.setIp(ip);
        droplet.setName(name);
        return droplet;
    }

    private Droplet getDroplet(long dropletId) {
        try {
            sleep(50000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        HttpClient client = HttpClientBuilder.create().build();
        HttpGet request = new HttpGet("https://api.digitalocean.com/v2/droplets/" + dropletId);
        request.setHeader(HttpHeaders.CONTENT_TYPE, "application/json");
        request.setHeader("Authorization", "Bearer ed3b42be2ee505a14ab8eb489c2ae36cfcffd6b1700ebb5d257ad79c1a3d901a");
        HttpResponse response;
        try {
            response = client.execute(request);
            String json = EntityUtils.toString(response.getEntity());
            Droplet droplet = parseDroplet(json);
            if (droplet == null) {
                System.out.println("Try to renew droplet IP");
                return getDroplet(dropletId);
            } else {
                return droplet;
            }
        } catch (IOException e) {
            try {
                sleep(30000);
            } catch (InterruptedException e1) {
                e1.printStackTrace();
                System.out.println("Try to renew droplet IP");
                return getDroplet(dropletId);
            }
            e.printStackTrace();
        }
        return null;
    }

}
