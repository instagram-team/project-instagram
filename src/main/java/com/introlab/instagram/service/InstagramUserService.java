package com.introlab.instagram.service;

import com.introlab.instagram.domain.InstagramUser;
import com.introlab.instagram.domain.SearchQuery;
import com.introlab.instagram.rpository.InstagramUserRepository;
import lombok.ToString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.aggregation.TypedAggregation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Service;

import java.util.List;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.*;

/**
 * @author vitalii.
 */
@Service
public class InstagramUserService {

    private final InstagramUserRepository userRepository;

    @Autowired
    public InstagramUserService(InstagramUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public void save(InstagramUser instagramUser) {
        userRepository.save(instagramUser);
    }

    public void save(Page<InstagramUser> instagramUser) {
        userRepository.save(instagramUser.getContent());
    }

    public Long countByQueryId(SearchQuery query) {
        return userRepository.countByTag(query.getId());
    }

    public Long count() {
        return userRepository.count();
    }

    public Long countDistinctByEmailExistsAndQueryId(SearchQuery query) {
        return userRepository.countDistinctByEmailContainsAndTag(query.getId());
    }

    @Autowired
    private MongoTemplate mongoTemplate;

    public void agregateByEmailExistsAndQueryId() {

        TypedAggregation<InstagramUser> agg = Aggregation.newAggregation(InstagramUser.class,
                unwind("tag"),
                match(Criteria.where("email").ne("")),
                group("tag.id").count().as("count"),
                project("count").and("_id").as("pageId")
        );
//        System.out.println(agg);
        AggregationResults<PostCount> aggregate = mongoTemplate.aggregate(agg, InstagramUser.class, PostCount.class);
        List<PostCount> mappedResults = aggregate.getMappedResults();
        for (PostCount postCount : mappedResults) {
            System.out.println(postCount);
        }


    }

    @ToString
    class PostCount {
        String pageId;
        int count;
    }

    public Long countDistinctByEmailExists() {
        return userRepository.countDistinctByEmailContains();
    }

    public List<InstagramUser> findByQuery(SearchQuery query) {
        return userRepository.findByTagForExport(query.getId());
    }

    public List<InstagramUser> findByQueryWithEmails(SearchQuery query) {
        return userRepository.dstinctByEmailContainsAndTag(query.getId());
    }

    public void deleteByQueryId(String id) {
        userRepository.deleteByQueryId(id);
    }

    public Page<InstagramUser> getListForSplittingName(int page) {
        return userRepository.findAll(new PageRequest(page, 10000, Sort.Direction.ASC, "_id"));
    }

    public void updateNames() {
        int i = 0;
        int totalPages = getListForSplittingName(0).getTotalPages();
        while (i < totalPages) {
            System.out.println("Start update");
            Page<InstagramUser> instagramUserPage = getListForSplittingName(i++);
            instagramUserPage.getContent().parallelStream().forEach(this::splitName);
            save(instagramUserPage);

            System.out.println(i+"/"+totalPages);
            System.out.println("1000 profiles updated!!!");
        }
    }

    public void splitName(InstagramUser instagramUser) {
        String cleanName = instagramUser.getFullName().replaceAll("[^AĄBCĆDEĘFGHIJKLŁMNŃOÓPRSŚTUWYZŹŻaąbcćdeęfghijklłmnńoóprsśtuwyzźżA-Za-zQVXÅÄÖqvxåäö\\s]+", "");
        if (cleanName != null && !cleanName.isEmpty()) {

            String[] split = cleanName.split("\\s+");

            if (split.length > 1) {
                instagramUser.setFirstName(split[0]);
                instagramUser.setLastName(split[1]);
            } else if (split.length != 0) {
                instagramUser.setFirstName(split[0]);
                instagramUser.setLastName("");
            }
        }
    }
}
