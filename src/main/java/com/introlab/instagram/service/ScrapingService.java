package com.introlab.instagram.service;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author vitalii.
 */
@Getter
@Service
public class ScrapingService {
    private final JSoupService JSoupService;

    private final BaseUserService baseUserService;

    private final InstagramUserService instagramUserService;

    private final SearchQueryService searchQueryService;




    @Autowired
    public ScrapingService(JSoupService JSoupService, BaseUserService baseUserService, InstagramUserService instagramUserService, SearchQueryService searchQueryService) {
        this.JSoupService = JSoupService;
        this.baseUserService = baseUserService;
        this.instagramUserService = instagramUserService;
        this.searchQueryService = searchQueryService;
    }
}
