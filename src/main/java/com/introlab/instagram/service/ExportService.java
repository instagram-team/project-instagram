package com.introlab.instagram.service;

import com.introlab.instagram.domain.InstagramUser;
import com.introlab.instagram.domain.InstagramUserDenormalized;
import com.introlab.instagram.domain.SearchQuery;
import org.springframework.stereotype.Service;
import org.supercsv.cellprocessor.Optional;
import org.supercsv.cellprocessor.ParseDouble;
import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.io.CsvBeanWriter;
import org.supercsv.io.ICsvBeanWriter;
import org.supercsv.prefs.CsvPreference;
import org.supercsv.util.CsvContext;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;

/**
 * @author vitalii.
 */
@Service
public class ExportService {

    public void exportUsers(List<InstagramUser> instagramUsers, String tag) {
        LocalDateTime dateTime = LocalDateTime.now();
        ICsvBeanWriter beanWriter = null;
        try {
            OutputStreamWriter stream = new OutputStreamWriter( new FileOutputStream(new File("exports/" + tag + "-" + dateTime + ".csv")), StandardCharsets.UTF_8);
//            FileWriter fileWriter = new FileWriter(stream);
            beanWriter = new CsvBeanWriter(stream, CsvPreference.STANDARD_PREFERENCE);

            // the header elements are used to map the bean values to each column (names must match)
            final String[] header = new String[]{
                    "Username",
                    "Full Name",
                    "Biography",
                    "Amount of Followers",
                    "Amount of Following",
                    "Amount of Photos",
                    "Scraped for tag",
                    "Email",
                    "External Link",
                    "Created Date",
                    "Country Block",
                    "Average amount Of Comments",
                    "Average amount Of Likes",
                    "Engagement Rate"
            };
            final String[] fields = new String[]{
                    "userName",
                    "fullName",
                    "description",
                    "amountOfFollowers",
                    "amountOfFollowing",
                    "amountOfPhotos",
                    "tag",
                    "email",
                    "externalLink",
                    "createdDate",
                    "countryBlock",
                    "amountOfComments",
                    "amountOfLikes",
                    "engagementRate"
            };
            final CellProcessor[] processors = getProcessors();
            beanWriter.writeHeader(header);
            for (InstagramUser customer : instagramUsers) {
                beanWriter.write(customer, fields, processors);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (beanWriter != null) {
                try {
                    beanWriter.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private static CellProcessor[] getProcessors() {

        return new CellProcessor[]{
                new Optional(),
                new Optional(),
                new Optional(),
                new Optional(),
                new Optional(),
                new Optional(),

                new CellProcessor() {
                    @Override
                    public <T> T execute(Object o, CsvContext csvContext) {
                        String tags = "";
                        for (Object o1 : LinkedHashSet.class.cast(o)) {
                            String tags1 = SearchQuery.class.cast(o1).getTags();
                            tags+="#"+tags1;
                        }

                        return (T)tags ;
                    }
                },

                new Optional(),
                new Optional(),
                new Optional(),
                new Optional(),
                new Optional(),
                new Optional(),
                new Optional(),
        };
    }

//    public void export(List<InstagramUser> instagramUsers){
//        exportUsers(instagramUsers);
//    }
}
