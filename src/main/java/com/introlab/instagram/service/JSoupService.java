package com.introlab.instagram.service;

import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.stereotype.Service;

import java.io.IOException;

import static java.lang.Thread.sleep;

/**
 * @author vitalii.
 */
@Service
public class JSoupService {

    private static final Logger logger = Logger.getLogger(JSoupService.class);

    public Document fetch(String url) {
        logger.info("Fetch url:\t" + url);
        try {
            return Jsoup.connect(url)
                    .timeout(15000)
                    .maxBodySize(0)
                    .ignoreContentType(true)
                    .get();
        } catch (IOException e) {
            logger.error("Error fetching url:\t" + url);
            try {
                sleep(5000);
                return Jsoup.connect(url)
                        .timeout(25000)
                        .maxBodySize(0)
                        .ignoreHttpErrors(true)
                        .ignoreContentType(true)
                        .get();
            } catch (IOException e1) {
                logger.error("Error fetching url:\t" + url+"\n"+e.getMessage(), e);
                return Jsoup.parse("");
            } catch (InterruptedException e1) {
                e1.printStackTrace();
            }
        }
        return Jsoup.parse("");
    }
}
