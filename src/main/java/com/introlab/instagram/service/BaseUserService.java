package com.introlab.instagram.service;

import com.introlab.instagram.domain.BaseUser;
import com.introlab.instagram.domain.SearchQuery;
import com.introlab.instagram.rpository.BaseUserRepository;
import com.introlab.instagram.rpository.SearchQueryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

/**
 * @author vitalii.
 */
@Service
public class BaseUserService {

    private final BaseUserRepository userRepository;

    private final SearchQueryRepository queryRepository;

    @Autowired
    public BaseUserService(BaseUserRepository userRepository, SearchQueryRepository queryRepository) {
        this.userRepository = userRepository;
        this.queryRepository = queryRepository;
    }

    public void save(BaseUser instagramUser) {
        userRepository.save(instagramUser);
    }

    public void save(Set<BaseUser> instagramUsers) {
        userRepository.save(instagramUsers);
    }

    public Long countBySearchQuery(SearchQuery query) {
        return userRepository.countByTag(query.getId());
    }

    public BaseUser findBiId(Long id) {
        return userRepository.findOne(id);
    }

    public List<BaseUser> getForDetailScraping() {
        return userRepository.findAllByScraped(false);
    }

    public List<BaseUser> getPackForScraping() {
        List<BaseUser> baseUsers = userRepository.findFirst100ByScraped(false);
        for (BaseUser baseUser : baseUsers) {
            baseUser.setScraped(true);
        }
        userRepository.save(baseUsers);
        return baseUsers;
    }

    public boolean isJobExist() {
        List<BaseUser> baseUsers = userRepository.findFirst10ByScraped(false);
        List<SearchQuery> notFinished = queryRepository.findByFinished(false);
        System.out.println("Checking for existing jobs.....");
        System.out.println("Count of not finished tags - " + notFinished.size());
        System.out.println("Count of users for scrape  - " + baseUsers.size());
        return isTagsForScrapePresent(notFinished) || isUsersForScrapePresent(baseUsers);
    }

    private boolean isUsersForScrapePresent(List<BaseUser> baseUsers) {
        return !baseUsers.isEmpty();
    }

    private boolean isTagsForScrapePresent(List<SearchQuery> notFinished) {
        return !notFinished.isEmpty();
    }

    public void deleteByQueryId(String id) {
        userRepository.deleteByQueryId(id);
    }
}
