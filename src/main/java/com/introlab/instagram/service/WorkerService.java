package com.introlab.instagram.service;

import com.introlab.instagram.domain.Droplet;
import com.introlab.instagram.domain.Token;
import com.introlab.instagram.rpository.DropletRepository;
import com.introlab.instagram.rpository.TokenRepository;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

import static java.lang.Thread.sleep;

/**
 * @author vitalii.
 */
@Service
public class WorkerService {

    private int maxWorkerCount = 7;

    private final DropletService dropletService;

    private final DropletRepository dropletRepository;

    private final TokenRepository tokenRepository;

    private final BaseUserService baseUserService;

    @Setter
    private boolean runned;

    @Autowired
    public WorkerService(DropletRepository dropletRepository, TokenRepository tokenRepository, DropletService dropletService, BaseUserService baseUserService) {
        this.dropletRepository = dropletRepository;
        this.tokenRepository = tokenRepository;
        this.dropletService = dropletService;
        this.baseUserService = baseUserService;
    }

    public Droplet getDropletByIp(String ip) {
        return dropletRepository.findById(ip);
    }

    public Token getAvailableToken(Long dropletId) {
        Token token = tokenRepository.findFirstByInUse(false);
        if (token != null) {
            token.setInUse(true);
            token.setDropletId(dropletId);
            tokenRepository.save(token);
            return token;
        }
        return null;
    }

    public List<Token> getFreeTokens(int count) {
        return tokenRepository.findFirst10ByInUse(false);
    }

    public void startWorkers() {
        if (runned) {
            return;
        }
        if (dropletService.listDropletsDataBase().size() < maxWorkerCount) {
            runned = true;
            for (int i = dropletService.listDropletsDataBase().size(); i < maxWorkerCount; i++) {
                List<Token> freeTokens = getFreeTokens(10);
                if (freeTokens.size() == 10) {
                    System.out.println("Attempt to start worker!!!");
                    Droplet droplet = dropletService.createDroplet("instagram-worker-" + i);
                    dropletRepository.save(droplet);
                    for (Token freeToken : freeTokens) {
                        freeToken.setInUse(true);
                        freeToken.setDropletId(droplet.getId());
                    }
                    tokenRepository.save(freeTokens);
                }
            }
        }
        dropWorkerIfNoJobsInNewThread();
    }

    //    @Scheduled(fixedDelay = 180000)
    public void dropWorkerIfNoJobsInNewThread() {

        new Thread(() -> {
            try {
                while (true) {
                    sleep(600000);
                    boolean noJobs = dropWorkersIfNoJobs();
                    if (noJobs) {
                        runned = false;
                        break;
                    }
                }
            } catch (InterruptedException | IOException e) {
                e.printStackTrace();
            }
        }).start();
    }

    private boolean dropWorkersIfNoJobs() throws IOException {
        if (!baseUserService.isJobExist()) {
            System.out.println("Stopping workers....");
            for (Droplet droplet : dropletService.listDropletsDataBase()) {
                dropletService.removeDroplet(droplet.getId());
                dropletService.removeFromDB(droplet);
                releaseTokens(droplet.getId());
            }
            return true;
        }
        return false;
    }

    public void releaseTokens(Long dropletId) {
        List<Token> tokens = tokenRepository.findByDropletId(dropletId);
        for (Token token : tokens) {
            token.setDropletId(0L);
            token.setInUse(false);
        }
        tokenRepository.save(tokens);
    }

    public void releaseAllTokens() {
        List<Token> tokens = tokenRepository.findAll();
        for (Token token : tokens) {
            token.setDropletId(0L);
            token.setInUse(false);
        }
        tokenRepository.save(tokens);
    }

    public void saveTokens(List<Token> tokens) {
        tokenRepository.save(tokens);
    }

    public void saveToken(Token token) {
        tokenRepository.save(token);
    }

    public List<Token> getAllTokens() {
        return tokenRepository.findAll();
    }
}
