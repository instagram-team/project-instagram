package com.introlab.instagram.config;

import com.introlab.instagram.domain.Token;
import com.introlab.instagram.service.WorkerService;
import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

/**
 * @author vitalii.
 */
@Component
public class Init {

    private final WorkerService workerService;

    @Autowired
    public Init(WorkerService workerService) throws IOException {
        this.workerService = workerService;
        List<Token> tokens = new ArrayList<>();

        Resource resource = new ClassPathResource("tokens.txt");
        BufferedReader reader = new BufferedReader(new InputStreamReader(resource.getInputStream()));
        reader.lines().forEach(token -> {
            Token tokenObject = new Token();
            tokenObject.setToken(token);
            tokens.add(tokenObject);
        });

        workerService.saveTokens(tokens);
    }
}
