package com.introlab.instagram.domain;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @author vitalii.
 */
@Getter
@Setter
public class Statistic {
    private Long allUsersCount;
    private Long allWithEmails;
    private List<SubStatistic> subStatistics;


}
