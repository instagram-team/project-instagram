package com.introlab.instagram.domain;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author vitalii.
 */
@Document
@Getter
@Setter
public class Token {
    @Id
    private String token;
    private Long dropletId;
    private boolean inUse;
}
//541299