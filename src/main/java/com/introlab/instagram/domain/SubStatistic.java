package com.introlab.instagram.domain;

import lombok.Getter;
import lombok.Setter;

/**
 * @author vitalii.
 */
@Setter
@Getter
public class SubStatistic {
    private String tag;
    private Long allUsersCount;
    private Long withEmails;
}
