package com.introlab.instagram.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Set;

/**
 * @author vitalii.
 */
@Setter
@Getter
@Document
@ToString
public class BaseUser {
    @Id
    private Long id;
    private String userName;
    private Set<SearchQuery> tag;
    private boolean scraped;
}