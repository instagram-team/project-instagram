package com.introlab.instagram.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author vitalii.
 */
@Getter
@Setter
@ToString
public class InstagramUserDenormalized {
    private long instaUserId;
    private String userName;
    private String fullName;
    private String description;
    private long amountOfFollowers;
    private long amountOfFollowing;
    private long amountOfPhotos;
    private String queryId;
    private String tag;
    private double engagementRate;
    private String profilePicUrl;
    private String profilePicUrlHD;
    private String email;
    private boolean privateProfile;
    private boolean verifiedProfile;
    private String externalLink;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private Date createdDate;
    private boolean countryBlock;
    private String link;
    private Long amountOfComments;
    private Long amountOfLikes;
    private String mediaLocation;
    private String mediaLink;
    private String mediaTags;

    public static List<InstagramUserDenormalized> denormalize(InstagramUser user) {
        if (user.getMedias().isEmpty()) {
            return Collections.singletonList(getInstagramUserDenormalizedWithoutMedia(user));
        } else {
            return user.getMedias().stream().map(media -> {
                InstagramUserDenormalized denormalized = getInstagramUserDenormalizedWithoutMedia(user);
                denormalized.mediaLocation = media.getLocation();
                denormalized.mediaLink = media.getLink();
                denormalized.mediaTags = media.getTags();
                return denormalized;
            }).collect(Collectors.toList());
        }
    }

    private static InstagramUserDenormalized getInstagramUserDenormalizedWithoutMedia(InstagramUser user) {
        InstagramUserDenormalized denormalized = new InstagramUserDenormalized();
//        denormalized.instaUserId = user.getInstaUserId();
        denormalized.userName = user.getUserName();
        denormalized.fullName = user.getFullName();
        denormalized.description = user.getDescription();
        denormalized.amountOfFollowers = user.getAmountOfFollowers();
        denormalized.amountOfFollowing = user.getAmountOfFollowing();
        denormalized.amountOfPhotos = user.getAmountOfPhotos();
//        denormalized.tag = user.getTag();
        denormalized.engagementRate = user.getEngagementRate();
        denormalized.profilePicUrl = user.getProfilePicUrl();
        denormalized.profilePicUrlHD = user.getProfilePicUrlHD();
        denormalized.email = user.getEmail();
        denormalized.privateProfile = user.isPrivateProfile();
        denormalized.verifiedProfile = user.isVerifiedProfile();
        denormalized.externalLink = user.getExternalLink();
        denormalized.createdDate = user.getCreatedDate();
        denormalized.countryBlock = user.isCountryBlock();
        denormalized.link = user.getLink();
        denormalized.amountOfComments = user.getAmountOfComments();
        denormalized.amountOfLikes = user.getAmountOfLikes();
        return denormalized;
    }
}
