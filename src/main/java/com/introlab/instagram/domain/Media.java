package com.introlab.instagram.domain;

import lombok.Getter;
import lombok.Setter;

/**
 * @author vitalii.
 */
@Getter
@Setter
public class Media {
    private String id;
    private String location;
    private String link;
    private String tags;
    private Double latitude;
    private Double longitude;


    public Media(String id, String location, String link, String tags, Double latitude, Double longitude) {
        this.id = id;
        this.location = location;
        this.link = link;
        this.tags = tags;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public Media(){
    }

}
