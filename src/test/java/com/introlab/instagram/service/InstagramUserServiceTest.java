package com.introlab.instagram.service;

import com.introlab.instagram.ProjectInstagramApplicationTests;
import com.introlab.instagram.domain.*;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

/**
 * @author vitalii.
 */
public class InstagramUserServiceTest extends ProjectInstagramApplicationTests{

    @Autowired
    private InstagramUserService userService;

    @Autowired
    private BaseUserService baseUserService;

    @Autowired
    private ExportService  exportService;

    @Autowired
    private SearchQueryService  searchQueryService;

    @Test
    public void test() {

        for (SearchQuery searchQuery : searchQueryService.findAll()) {
            List<InstagramUser> instagramUsers = userService.findByQuery(searchQuery);
//            List<InstagramUserDenormalized> denormalizedList = new ArrayList<>();
//            for (InstagramUser instagramUser : instagramUsers) {
//                denormalizedList.addAll(InstagramUserDenormalized.denormalize(instagramUser));
//            }
            exportService.exportUsers(instagramUsers, searchQuery.getTags());
        }
    }

    @Test
    public void test2(){
        userService.agregateByEmailExistsAndQueryId();
    }
}