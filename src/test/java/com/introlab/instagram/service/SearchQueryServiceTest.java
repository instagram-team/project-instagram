package com.introlab.instagram.service;

import com.introlab.instagram.ProjectInstagramApplicationTests;
import com.introlab.instagram.domain.SearchQuery;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.*;

/**
 * @author vitalii.
 */
public class SearchQueryServiceTest extends ProjectInstagramApplicationTests{

    @Autowired
    private SearchQueryService searchQueryService;

    @Test
    public void save() throws Exception {

        SearchQuery searchQuery = new SearchQuery();
        searchQuery.setId("liverpool2017");
        searchQuery.setTags("liverpool");
        searchQueryService.save(searchQuery);

        searchQuery = new SearchQuery();
        searchQuery.setId("cat");
        searchQuery.setTags("cat");
        searchQueryService.save(searchQuery);

        searchQuery = new SearchQuery();
        searchQuery.setId("food");
        searchQuery.setTags("food");
        searchQueryService.save(searchQuery);

        searchQuery = new SearchQuery();
        searchQuery.setId("messi");
        searchQuery.setTags("messi");
        searchQueryService.save(searchQuery);

        searchQuery = new SearchQuery();
        searchQuery.setId("love");
        searchQuery.setTags("love");
        searchQueryService.save(searchQuery);

    }

}