package com.introlab.instagram.service;

import com.introlab.instagram.ProjectInstagramApplicationTests;
import com.introlab.instagram.domain.Droplet;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;

/**
 * @author vitalii.
 */

public class DropletServiceTest extends ProjectInstagramApplicationTests{


    @Autowired
    private DropletService dropletService;

    @Test
    public void test() throws IOException {

        Droplet droplet = dropletService.createDroplet("test3");
        System.out.println(droplet);
//        dropletService.listDroplets();
//        dropletService.removeDroplet(52803262L);
    }
}