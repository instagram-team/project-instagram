package com.introlab.instagram.service;

import com.introlab.instagram.ProjectInstagramApplicationTests;
import org.jsoup.nodes.Document;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.*;

/**
 * @author vitalii.
 */
public class JSoupServiceTest extends ProjectInstagramApplicationTests{


    @Autowired
    JSoupService jSoupService;

    @Test
    public void test(){
        Document fetch = jSoupService.fetch("https://api.instagram.com/v1/tags/köksrenovering/media/recent?access_token=5634873959.e029fea.07fe7b1ed9ea4df9bdfaa995ff848b58&count=33&max_tag_id=AQAR1imS-8ez9pZowUda8xaMld6R1IrcoGWUpWoil4aFtAFEob1LuJ0aXo_NAfBtxhkKGUGeSb6bpAKGaT8feH5ps0JUNjS5AXFXj6WtwHRzSAR9B0lOmT-j91ZLG2YlPrk");
        System.out.println(fetch);
    }
}